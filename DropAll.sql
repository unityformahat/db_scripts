DROP TABLE UNITS CASCADE;
DROP TABLE GAFS CASCADE;
DROP TABLE TEAMS CASCADE;
DROP TABLE DEVELOPERS CASCADE;
DROP TABLE JIRA_PROJECTS CASCADE;
DROP TABLE BAMBOO_PROJECTS CASCADE;
DROP TABLE BITBUCKET_PROJECTS CASCADE;
DROP TABLE DEVELOPMENT_PROJECTS CASCADE;
DROP TABLE SPRINTS CASCADE;
DROP TABLE INITIATIVES CASCADE;
DROP TABLE PORTFOLIO_EPICS CASCADE;
DROP TABLE EPICS CASCADE;
DROP TABLE STORIES CASCADE;
DROP TABLE SUB_TASKS CASCADE;
DROP TABLE ISSUE_LEVELS CASCADE;
DROP TABLE WORK_REPORTS CASCADE;