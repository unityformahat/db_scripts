CREATE TABLE UNITS (
	UnitID int PRIMARY KEY,
	HebrewName varchar(255),
	EnglishName varchar(255)
);
CREATE TABLE GAFS (
	GafID int PRIMARY KEY,
	HebrewName varchar(255),
	EnglishName varchar(255),
	UnitID int REFERENCES UNITS(UnitID)
);
CREATE TABLE TEAMS (
	TeamID int PRIMARY KEY,
	HebrewName varchar(255),
	EnglishName varchar(255),
	GafID int REFERENCES GAFS(GafID)
);
CREATE TABLE DEVELOPERS (
	DeveloperID varchar(255) PRIMARY KEY,
	Name varchar(255),
	Proffession varchar(255),
	Experience varchar(255),
	Education varchar(255),
	TeamID int REFERENCES TEAMS(TeamID)
);
CREATE TABLE JIRA_PROJECTS (
	JiraProjectID int PRIMARY KEY,
	ProjectKey varchar(255),
	Name varchar(255),
	ProjectType varchar(255)
);
CREATE TABLE BAMBOO_PROJECTS (
	BambooProjectID int PRIMARY KEY,
	Name varchar(255)
);
CREATE TABLE BITBUCKET_PROJECTS (
	BitBucketProjectID int PRIMARY KEY,
	Name varchar(255)
);
CREATE TABLE DEVELOPMENT_PROJECTS (
	ProjectID int PRIMARY KEY,
	Name varchar(255),
	JiraProjectID int REFERENCES JIRA_PROJECTS(JiraProjectID),
	BambooProjectID int REFERENCES BAMBOO_PROJECTS(BambooProjectID),
	BitBucketProjectID int REFERENCES BITBUCKET_PROJECTS(BitBucketProjectID),
	GafID int REFERENCES GAFS(GafID)
);
CREATE TABLE SPRINTS (
	SprintID int PRIMARY KEY,
	Name varchar(255),
	StartTIme date,
	EndTime date,
	PiID int
);
CREATE TABLE INITIATIVES (
	InitiativeID varchar(255) PRIMARY KEY,
	Name varchar(255),
	CreationTime date,
	ReporterID varchar(255) REFERENCES DEVELOPERS(DeveloperID),
	ProjectID int REFERENCES JIRA_PROJECTS(JiraProjectID),
	Status varchar(255),
	Assigne varchar(255) REFERENCES DEVELOPERS(DeveloperID)
);
CREATE TABLE PORTFOLIO_EPICS (
	PortfolioEpicID varchar(255) PRIMARY KEY,
	Name varchar(255),
	CreationTime date,
	ReporterID varchar(255) REFERENCES DEVELOPERS(DeveloperID),
	ProjectID int REFERENCES JIRA_PROJECTS(JiraProjectID),
	InitiativeID varchar(255) REFERENCES INITIATIVES(InitiativeID),
	Status varchar(255),
	Assigne varchar(255) REFERENCES DEVELOPERS(DeveloperID)
);
CREATE TABLE EPICS (
	EpicID varchar(255) PRIMARY KEY,
	Name varchar(255),
	CreationTime date,
	ReporterID varchar(255) REFERENCES DEVELOPERS(DeveloperID),
	ProjectID int REFERENCES JIRA_PROJECTS(JiraProjectID),
	PortfolioEpicID varchar(255) REFERENCES PORTFOLIO_EPICS(PortfolioEpicID),
	Status varchar(255),
	Assigne varchar(255) REFERENCES DEVELOPERS(DeveloperID)
);
CREATE TABLE STORIES (
	StoryID varchar(255) PRIMARY KEY,
	Name varchar(255),
	CreationTime date,
	ReporterID varchar(255) REFERENCES DEVELOPERS(DeveloperID),
	ProjectID int REFERENCES JIRA_PROJECTS(JiraProjectID),
	EpicID varchar(255) REFERENCES EPICS(EpicID),
	SprintID int REFERENCES SPRINTS(SprintID),
	Status varchar(255),
	Assigne varchar(255) REFERENCES DEVELOPERS(DeveloperID),
  	IssueType varchar(255),
  	Priority varchar(255)
);
CREATE TABLE SUB_TASKS (
	SubTaskID varchar(255) PRIMARY KEY,
	Name varchar(255),
	CreationTime date,
	ReporterID varchar(255) REFERENCES DEVELOPERS(DeveloperID),
	ProjectID int REFERENCES JIRA_PROJECTS(JiraProjectID),
	StoryID varchar(255) REFERENCES STORIES(StoryID),
	Status varchar(255),
	Assigne varchar(255) REFERENCES DEVELOPERS(DeveloperID)
);
CREATE TABLE WORK_REPORTS (
	WorkReportID int PRIMARY KEY,
	StoryID varchar(255) REFERENCES STORIES(StoryID),
	TaskID varchar(255) REFERENCES SUB_TASKS(SubTaskID),
	Quantity int,
	ReporterID varchar(255) REFERENCES DEVELOPERS(DeveloperID),
	DateReported date
);
CREATE TABLE ISSUE_LEVELS (
	IssueTypeID int PRIMARY KEY,
	Name varchar(255),
	LevelName varchar(255)
);